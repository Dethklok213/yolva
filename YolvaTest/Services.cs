﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YolvaTest
{
    enum Services
    {
        [Display(Name = "Open Street Map")]
        OpenStreetMap = 1
    }


    public static class ServicesStringBuilder
    {
        public static string BuildServicesString()
        {
            var index = 1;
            var text = "Выберите геосервис\n";
            foreach (var value in Enum.GetValues(typeof(Services)))
            {
                text += $"{index}. {Enum.GetName(typeof(Services), value)}\n";
                index++;
            }
            return text;
        }
    }
}
