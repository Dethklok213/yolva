﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace YolvaTest
{



    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите адрес");
            var place = Console.ReadLine();
            Console.WriteLine(ServicesStringBuilder.BuildServicesString()); 
            Services service = (Services)Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите частоту точек");
            var accuracy = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите имя файла для сохранения");
            var fileName = Console.ReadLine();
            var geoData = GetCoordinates(service, place);
            if (accuracy - 1 < 0 || geoData.Count < accuracy - 1)
                Console.WriteLine("Запрашиваемая частота точек за пределами размера массива");
            else
                SaveToFile(DecimateCoordinates(accuracy, geoData), fileName);
        }

        private static List<List<double>> GetCoordinates(Services service, string place)
        {
            var coordinates = new List<List<double>>();
            var url = "";
            switch (service)
            {
                case Services.OpenStreetMap:
                {
                    url = $"https://nominatim.openstreetmap.org/search?q=" + place + "&format=json&polygon_geojson=1";
                    break;
                }
            }
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = ".NET Framework Test Client";
            var response = request.GetResponse();
            var data = response.GetResponseStream();
            var reader = new StreamReader(data);
            string responseFromServer = reader.ReadToEnd();
            response.Close();
            switch (service)
            {
                case Services.OpenStreetMap:
                {
                        //var results = JsonConvert
                        //    .DeserializeObject<List<OpenStreetMapJsonResult>>(responseFromServer); // Так как OpenStreetMap представляет в качестве координат массивы разных уровней,
                        //geojson = results.First().Geojson;                                         //  то от десериализации я отказался в пользу regex
                        var coordinatePairs = Regex.Matches(responseFromServer, "\\[(\\d*?\\.\\d*?\\,\\d*?\\.\\d*?)\\]");
                        foreach (var pair in coordinatePairs)
                        {
                            var point = new List<double>();
                            string[] split = pair.ToString().Split(",");
                            foreach (var coordWithBracketsAndDot in split)
                            {
                                var coord = coordWithBracketsAndDot
                                    .Replace("[", string.Empty).Replace("]", string.Empty)
                                    .Replace(".",",");
                                point.Add(Convert.ToDouble(coord));
                            }
                            coordinates.Add(point);
                        }
                        break;
                }
            }
            return coordinates;
        }

        private static List<List<double>> DecimateCoordinates(int accuracy, List<List<double>> coordinates)
        {
            if (accuracy == 1)
                return coordinates;
            var decimatedCoordinates = new List<List<double>>();
            for (int i = accuracy - 1; i < coordinates.Count; i += accuracy)
            {
                decimatedCoordinates.Add(coordinates[i]);
            }
            return decimatedCoordinates;
        }

        private static void SaveToFile(List<List<double>> coords, string fileName)
        {
            var text = JsonConvert.SerializeObject(coords);
            using StreamWriter writer = new StreamWriter($"{fileName}.txt");
                writer.WriteLine(text);
        }
    }
}
